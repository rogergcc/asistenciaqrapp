package com.rogergcc.asistenciaqrapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.rogergcc.asistenciaqrapp.servicios.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class VerificationNumberActivity extends AppCompatActivity {
    public static final String TAG = VerificationNumberActivity.class.getSimpleName();
    public static final String BARCODE_KEY = "BARCODE";
    private static final int REQUEST_CODE_NUMBER = 1008;
    private final int MY_PERMISSION_REQUEST_CAMERA = 1001;
    EditText input_phonenumber, _dniText, _nameText, _emailText;
    String emailst, dnist, namest, phonenumber;
    private PrefsHelper prefs;
    private TextView tvDatosResultado;
    private FloatingActionButton fabScanear;
    private Barcode barcodeResult;
    private String datosGuardados;
    private ProgressDialog pd;
    private LinearLayout llFormulario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_number);
        tvDatosResultado = findViewById(R.id.tvDatosResultado);
        fabScanear = findViewById(R.id.fabScanear);
        llFormulario = findViewById(R.id.llFormulario);


        pd = new ProgressDialog(this);



        _dniText = findViewById(R.id.input_dni);
        _nameText = findViewById(R.id.input_name);
        _emailText = findViewById(R.id.input_email);


        String correo = getPrefs().getCorreo(null);
        String dni = getPrefs().getDni(null);
        String nombre = getPrefs().getNombre(null);


        if (getPrefs().getVerified(false)) {
            llFormulario.setVisibility(View.GONE);
        }else {
            llFormulario.setVisibility(View.VISIBLE);
        }
        if (correo != null) {
            String datosGuardados = nombre + "\n" +
                    dni + "\n" +
                    correo + "\n";
            _dniText.setText(dni);
            _nameText.setText(nombre);
            _emailText.setText(correo);

            tvDatosResultado.setText(datosGuardados);

        }


        if (savedInstanceState != null) {
            Barcode restoredBarcode = savedInstanceState.getParcelable(BARCODE_KEY);
            if (restoredBarcode != null) {
                tvDatosResultado.setText(restoredBarcode.rawValue);
                barcodeResult = restoredBarcode;
            }
        }

    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, getResources().getString(R.string.camera_permission_granted));
            startScanningBarcode();
        } else {
            requestCameraPermission();

        }
    }


    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

            ActivityCompat.requestPermissions(VerificationNumberActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSION_REQUEST_CAMERA);

        } else {
            ActivityCompat.requestPermissions(VerificationNumberActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSION_REQUEST_CAMERA);
        }
    }


    private String obtenerFechaActualAMPM() {

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a", Locale.getDefault());
        return df.format(new Date());
    }

    private String obtenerFechaActual24HRS() {

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
        return df.format(new Date());
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BARCODE_KEY, barcodeResult);
        super.onSaveInstanceState(outState);
    }


    protected PrefsHelper getPrefs() {
        if (prefs == null) {
            prefs = new PrefsHelper(this);
        }
        return prefs;
    }


    public void actionQuitarNumero(View view) {
        String nrCelularGuardado = getPrefs().getPhoneNumber(null);
        if (nrCelularGuardado != null) {
            getPrefs().removePhoneNumber();
            tvDatosResultado.setText("NR CELULAR: -");
        }

    }

    private void startScanningBarcode() {
        final String defaultPhone = getPrefs().getPhoneNumber(null);
        final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                .withActivity(VerificationNumberActivity.this)
                .withEnableAutoFocus(true)
                .withBleepEnabled(true)
                .withBackfacingCamera()
                .withCenterTracker()
                .withOnlyQRCodeScanning()
                .withText("Scaneando...")
                .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                    @Override
                    public void onResult(Barcode barcode) {
                        barcodeResult = barcode;

                        String resultadoScanedadoQr = barcode.rawValue;
                        String correo = getPrefs().getCorreo(null);
                        String dni = getPrefs().getDni(null);
                        String nombre = getPrefs().getNombre(null);
                        String datosAsistenciaGuardado = "";

                        String fechaRegistroAsistencia = obtenerFechaActualAMPM();
                        if (correo != null) {
                            String datosGuardados = nombre + "\n" +
                                    dni + "\n" +
                                    correo + "\n";


                            datosAsistenciaGuardado = datosGuardados + "\n" + fechaRegistroAsistencia;



                            //showDialog(datosGuardados, obtenerFechaActualAMPM());

                            servicioMarcarAsistencia(datosAsistenciaGuardado,nombre,correo,fechaRegistroAsistencia,dni,resultadoScanedadoQr);
                        }

                    }
                })
                .build();

        materialBarcodeScanner.startScan();
    }

    public void clickFabScanear(View view) {


        if (validarDatosIngresados()) {
//            onSignupFailed();
            return;

        }
//        String correo = getPrefs().getCorreo(null);
//        String dni = getPrefs().getDni(null);
//        String nombre = getPrefs().getNombre(null);


        if (!getPrefs().getVerified(false)) {
            Toast.makeText(this, "Primero debe registrar sus datos", Toast.LENGTH_SHORT).show();
            return;
        }

        checkPermission();


    }

    public boolean validarDatosIngresados() {
        boolean datosValidos = true;

        namest = _nameText.getText().toString();
        emailst = _emailText.getText().toString();
        dnist = _dniText.getText().toString();

        if (namest.isEmpty() || namest.length() < 4) {
            _nameText.setError("Almenos 4 letras");
            datosValidos = false;
        } else {
            _nameText.setError(null);
        }

        if (emailst.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailst).matches()) {
            _emailText.setError("ingrese un correo válido");
            _emailText.setFocusable(true);
            datosValidos = false;
        } else {
            _emailText.setError(null);
        }

        if (dnist.length() != 8) {
            _dniText.setError("dni debe tener 8 dígitos");
            datosValidos = false;
        } else {
            _dniText.setError(null);
        }

        return !datosValidos;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSION_REQUEST_CAMERA && grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startScanningBarcode();
        } else {
            Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.sorry_for_not_permission), Snackbar.LENGTH_SHORT)
                    .show();
        }


    }

    public void actionGuardarDatos(View view) {


        if (validarDatosIngresados()) {
//            onSignupFailed();
            return;
        }

        getPrefs().setCorreo(emailst);
        getPrefs().setNombre(namest);
        getPrefs().setDni(dnist);
        getPrefs().setVerified(true);

        datosGuardados = namest + "\n" +
                dnist + "\n" +
                emailst + "\n";

        tvDatosResultado.setText(datosGuardados);
        Toast.makeText(this, "Datos Guardardos", Toast.LENGTH_SHORT).show();

        llFormulario.setVisibility(View.GONE);
    }

    private void showDialog(final String tituloDialog, final String scanContent, final String currentTime) {
        AlertDialog.Builder builder = new AlertDialog.Builder(VerificationNumberActivity.this);

        builder.setMessage(scanContent )
                .setTitle(tituloDialog);

        builder.setIcon(R.drawable.ic_baseline_save_24);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                DatabaseHelper databaseHelper = new DatabaseHelper(context);
//                databaseHelper.addProduct(new Product(scanContent,currentTime,currentDate));
//                Toast.makeText(VerificationNumberActivity.this, "Saved", Toast.LENGTH_SHORT).show();
//                viewPager.setCurrentItem(1);


            }
        });
//        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                Toast.makeText(VerificationNumberActivity.this, "Not Saved", Toast.LENGTH_SHORT).show();
//            }
//        });
        //builder.setCancelable(false);

        builder.show();
    }

    public void servicioMarcarAsistencia(
            String datosAsistenciaGuardado,
            String nombre,
            String correo,
            String fechaAsistencia,
            String dni,
            String respuestaQr
    ) {

        pd.setTitle("Enviando");
        pd.setMessage("Registrando Asistencia");
        pd.setCancelable(false);
        pd.show();
        String URLSERVICIO = "http://app.etebes.info/user1.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLSERVICIO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        if (response == null) {
                            Toast.makeText(VerificationNumberActivity.this, "No hay respuesta del servidor", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        //Toast.makeText(VerificationNumberActivity.this, response.toString(), Toast.LENGTH_SHORT).show();



                        try {
                            //converting response to json object
                            JSONObject jsonObject = new JSONObject(response);

                            //if no error in response
                            if (jsonObject.getInt("status")==1) {
                                String dni=jsonObject.getString("dni");
                                String nombre=jsonObject.getString("nombre");
                                String fecha=jsonObject.getString("fecha");
                                String hora=jsonObject.getString("hora");

                                String datosGuarDatosRespuesta = nombre
                                        +"\n"+dni
                                        +"\n"+fecha+ " " + hora;

//                                Log.e("Entro", "entro");
                                tvDatosResultado.setText(datosGuarDatosRespuesta);
                                showDialog("Se registro su asistencia",datosGuarDatosRespuesta, "");
                            } else {
                                String message=jsonObject.getString("message");
                                //Toast.makeText(getApplicationContext(), jsonObject.getString("mensaje"), Toast.LENGTH_SHORT).show();
                                showDialog(message,"", "");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Log.e("Error", "Error Tiempo Respuest");
                            showDialog("Algo ocurrio","", "");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nombre", nombre);
                params.put("correo", correo);
                params.put("fechaAsistencia", fechaAsistencia);
                params.put("dni", dni);
                params.put("respuestaQr", respuestaQr);
                return params;
            }
        };

        //AppSin
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

}